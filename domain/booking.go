package domain

import (
	null "gopkg.in/guregu/null.v3"
	"github.com/satori/go.uuid"
)

// Definition of all struct which represent a projection of data in the database

type TimeSlot struct {
	EndingHour   int `json:"endingHour"`
	StartingHour int `json:"startingHour"`
}

type MeetingRoom struct {
	MeetingRoomID 	string `json:"meetingRoomId"`
	MeetingRoomName null.String `json:"meetingRoomName"`
}

type User struct {
	UserID 		string `json:"userId"`
	UserEmail 	string `json:"userEmail"`
	Name 		null.String `json:"name"`
}

type Booking struct {
	BookingID 		uuid.UUID`json:"bookingId"`
	EndingHour   	int 	`json:"endingHour"`
	StartingHour 	int 	`json:"startingHour"`
	TenantID 		string  `json:"tenantId"`
	MeetingRoomName string 	`json:"meetingRoomName"`
	UserEmail		string 	`json:"userEmail"`
}

type RoomBooking struct {
	MeetingRoomName string 	`json:"meetingRoomName"`
	Bookings 		[]*Booking `json:"bookings"`
}

type ConsultingRepository interface {
	Connect(dns string)
	FindByBookingId(bookingID uuid.UUID) (*Booking, error)
	FindByMeetingRoomName(meetingRoomName string) ([]*Booking, error)
	Save(*Booking) (err error)
	Delete(bookingID uuid.UUID) (err error)
}

type BookingNotFound struct {
}

func (BookingNotFound) Error() string {
	return "meeting room not found"
}
