package domain

// Definition of all struct which represent Event retrieve from kafka message broker

const (
	CREATED    	string = "CREATED"
	DELETED 	string = "DELETED"
)

type Event struct {
	UUID          string `json:"uuid"`
	Type          string `json:"type"`
	CorrelationID string `json:"correlationId"`
	TenantID      string `json:"tenantId"`
}

type BookingUpdatedEvent struct {
	Event
	BookingState  string 				`json:"bookingState"`
	BookingEvent  BookingEvent 			`json:"booking"`
	UserEvent UserEvent                	`json:"user"`
	MeetingRoomEvent MeetingRoomEvent 	`json:"meetingRoom"`
}

type BookingEvent struct {
	BookingID struct {
		UUID string `json:"uuid"`
	} `json:"bookingId"`
	TimeSlot TimeSlot `json:"timeSlot"`
	MeetingRoomID struct {
		UUID string `json:"uuid"`
	} `json:"meetingRoomId"`
	UserID struct {
		UUID string `json:"uuid"`
	} `json:"userId"`
}

type UserEvent struct {
	UserID struct {
		UUID string `json:"uuid"`
	} `json:"userId"`
	UserEmail struct {
		Email string `json:"email"`
	} `json:"userEmail"`
	Name struct {
		Name string `json:"name"`
	} `json:"name"`
}

type MeetingRoomEvent struct {
	MeetingRoomID struct {
		UUID string `json:"uuid"`
	} `json:"meetingRoomId"`
	MeetingRoomName struct {
		Name string `json:"name"`
	} `json:"meetingRoomName"`
}

type EventConsultingService interface {
	AddHandler(topic string, f func(data []byte) error)
}
