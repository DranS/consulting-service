FROM drans/alpine-librdkafka-golang AS build-env

ADD . /go/src/consulting-service
WORKDIR /go/src/consulting-service
RUN go get -u github.com/golang/dep/cmd/dep
RUN dep ensure

RUN go build -o microservice

FROM drans/alpine-librdkafka-golang

WORKDIR /app
COPY --from=build-env /go/src/consulting-service/microservice /app/
ENTRYPOINT /app/microservice
EXPOSE 8080
