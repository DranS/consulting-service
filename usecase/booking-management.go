package usecase

import (
	"consulting-service/domain"
	"consulting-service/log-factory"

	"encoding/json"
	"github.com/satori/go.uuid"
)

const (
	serviceName = "consulting-usecase"

)

// Global Var
var (
	logger = log_factory.LoggerFactory(serviceName)
)

type BookingManagementService struct {
	Consulting   domain.ConsultingRepository
}

//	Handle Event from kafka topic
//	Retrieve field "bookingState" from the Event and root depending of his value
//	CREATED => save the booking
//	DELETED => remove the booking
//	Update database with this booking Event
func (service BookingManagementService) ApplyBookingUpdated(event []byte) (err error) {
	logger.Debug("Apply BookingUpdated")
	bookingUpdatedEvent := domain.BookingUpdatedEvent{}
	err = json.Unmarshal(event, &bookingUpdatedEvent)

	logger.Debug("event parse : ", string(event))

	if err != nil {
		return
	}

	bookingState := bookingUpdatedEvent.BookingState

	switch bookingState {
	case domain.CREATED:
		service.bookingCreated(bookingUpdatedEvent)
	case domain.DELETED:
		service.bookingDeleted(bookingUpdatedEvent)
	}

	return
}

//	Handle HTTP request to return a list of booking for a meeting room
//	Find in repository a list of booking link to this meeting room
func (service BookingManagementService) GetRoomBooking(meetingRoomName string) (roomBooking *domain.RoomBooking, err error) {

	bookings, err := service.Consulting.FindByMeetingRoomName(meetingRoomName)
	if err != nil {
		logger.Error("cannot find any Booking for meeting Room : ", err.Error())
		return
	}
	logger.Debug("build roomBooking struct")
	return &domain.RoomBooking{meetingRoomName, bookings}, err
}


// Function to save the booking in repository from BookingUpdated Event
func (service BookingManagementService) bookingCreated(event domain.BookingUpdatedEvent){
	var err error

	// Booking creation
	booking := &domain.Booking{}
	booking.BookingID, err = uuid.FromString(event.BookingEvent.BookingID.UUID)
	if err != nil {
		logger.Error("cannot convert BookingID into UUID : ", err.Error())
		return
	}
	booking.StartingHour = event.BookingEvent.TimeSlot.StartingHour
	booking.EndingHour= event.BookingEvent.TimeSlot.EndingHour
	booking.TenantID = event.TenantID
	booking.MeetingRoomName = event.MeetingRoomEvent.MeetingRoomName.Name
	booking.UserEmail = event.UserEvent.UserEmail.Email

	err = service.Consulting.Save(booking)
	if err != nil {
		logger.Error("cannot save the booking : ", err.Error())
		return
	}
}

// Function to delete the booking in repository from BookingUpdated Event
func (service BookingManagementService) bookingDeleted(event domain.BookingUpdatedEvent){
	bookingID, err := uuid.FromString(event.BookingEvent.BookingID.UUID)
	if err != nil {
		logger.Error("cannot convert BookingID into UUID : ", err.Error())
		return
	}
	err = service.Consulting.Delete(bookingID)
	if err != nil {
		logger.Error("cannot save the booking : ", err.Error())
		return
	}
}


