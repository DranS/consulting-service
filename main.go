package main

import (
	"consulting-service/domain"
	"consulting-service/infrastructure"
	"consulting-service/usecase"
	"net/http"
	"os"

	"consulting-service/log-factory"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

const (
	serviceName = "consulting-service"
)

// Define global Var
var (
	logger                   = log_factory.LoggerFactory(serviceName)
	consultingRepository     domain.ConsultingRepository
	consultingService 		 domain.EventConsultingService

	bookingManagementService usecase.BookingManagementService

	listen             = ":8080"
	err                error
)

// Retrieved Environment variables to set-up the service
var (
	brokerUrl          = os.Getenv("BROKER_KAFKA_URL")
	datasource         = os.Getenv("DATASOURCE")
)

func main() {
	// Initialize services
	services()
	// Initialize event handlers
	eventHandlers()
	// configure HTTP routing and start server
	httpServer()
}

func services() {
	// Define the database and setup the database connection
	consultingRepository = infrastructure.NewConsultingRepository()
	go consultingRepository.Connect(datasource)

	// Define the kafka message broker
	consultingService, err = infrastructure.NewKafkaConsultingService(brokerUrl, serviceName)
	if err != nil {
		logger.Error(err.Error())
	}

	// Define the booking management service which handles the Booking event and the consulting request
	bookingManagementService = usecase.BookingManagementService{
		Consulting: consultingRepository,
	}
}

func eventHandlers() {
	logger.Debug("Registering event handlers")

	// setup the kafka connection and define the Handler function to manage the event from "domain.booking" topic
	consultingService.AddHandler("domain.booking", bookingManagementService.ApplyBookingUpdated)

}

func httpServer() {
	// Define the rooter for dispatch request through Handler function
	r := mux.NewRouter()

	// Map the URI with the Handler function
	r.HandleFunc("/get/booking", infrastructure.HandleGetRoomBooking(bookingManagementService)).Methods("POST")

	// Map the URI with the Handler function
	r.HandleFunc("/manage/health", infrastructure.HealtCheck).Methods("GET")

	// Start the HTTP Web service
	logger.Info("Starting HTTP server")
	error := http.ListenAndServe(listen, r)
	logger.Error("err", error)
}
