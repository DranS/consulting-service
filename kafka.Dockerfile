FROM golang:alpine

# See https://hub.docker.com/r/astronomerio/alpine-librdkafka-golang/~/dockerfile/
ARG LIBRESSL_VERSION=2.7
ARG LIBRDKAFKA_VERSION=0.11.4-r1

RUN apk update
RUN apk add libressl${LIBRESSL_VERSION}-libcrypto libressl${LIBRESSL_VERSION}-libssl --update-cache --repository http://nl.alpinelinux.org/alpine/edge/main
RUN apk add librdkafka=${LIBRDKAFKA_VERSION} --update-cache --repository http://nl.alpinelinux.org/alpine/edge/community
RUN apk add librdkafka-dev=${LIBRDKAFKA_VERSION} --update-cache --repository http://nl.alpinelinux.org/alpine/edge/community
RUN apk add git openssh openssh-client openssl yajl-dev zlib-dev cyrus-sasl-dev openssl-dev build-base coreutils wget
