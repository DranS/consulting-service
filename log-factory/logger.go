package log_factory

import (
	"os"
	log "github.com/sirupsen/logrus"
)

// Generic function to instantiate the logger
func LoggerFactory(loggerName string) *log.Entry {
	// Log as Text ASCII format.
	log.SetFormatter(&log.TextFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	log.SetLevel(log.DebugLevel)
	logger := log.WithFields(log.Fields{
		"service": loggerName,
	})
	return logger
}
