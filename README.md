# Consulting service

The consulting service manage the request (**query**) to consult the status (which time slot are booked and which are not) for **all meeting room** of the building. 



##### Get the list of meeting room booked

To consult the status of a meeting room, the `client` need to call the URI `/consulting/get/booking` with a JSON like :

```json
{
	"meetingRoomName": "P01"
}
```

- The consulting service finds in the database all booking link to this meeting-room
- The consulting service returns a JSON body with the list of all booking for this meeting-room



## Technical characteristic

The consulting service was developed, with in mind, to be **asynchronously** with the booking service and **be decouple** of all booking service instances. 

The consulting service has to manage the consulting of the status of **all meeting rooms** of the building. 

To **not modify the consulting service** if we add a **new instance of booking service** for new company in the booking system, we use an [Event Driven model](https://martinfowler.com/eaaDev/EventCollaboration.html).

The consulting service listens **events** from a `topic` (queue) of the **message broker** where all **bookings state change are published**. 

For each event, the consulting service update his database depends from the type of `bookingState` :

* `CREATED` : Save in the database the new booking with the time slot, meeting room name and user email
* `DELETED` : Delete from the database the booking which is canceled



To configure the consulting service, environment variables are :

```yaml
- name: BROKER_KAFKA_URL	 # URL and PORT of the message broker kafka
- name: DATASOURCE			# URL, PORT, Username, Password and schema of the database 
```



For example, for the [booking solution](https://gitlab.com/DranS/booking-solution), the consulting service configuration are :

```yaml
- name: BROKER_KAFKA_URL
	value: kafka:9092
- name: DATASOURCE
	value: root:admin@tcp(db:3306)/consulting-service
```



## Build consulting service

#### Requirement

Before building the consulting service, you need to :

- Have **Docker** (or a **Docker Machine**) on your system with access to the `docker` command

To use an efficient kafka library, we need to install the library `librdkafka`  in the `Docker` from [confluent repository](https://github.com/confluentinc/confluent-kafka-go). To avoid to install the library `librdkafka` at each build of the consulting  service, we build a `Docker` image which contains the library.

The `kafka.Dockerfile` file is :

```dockerfile
FROM golang:alpine

# See https://hub.docker.com/r/astronomerio/alpine-librdkafka-golang/~/dockerfile/
ARG LIBRESSL_VERSION=2.7
ARG LIBRDKAFKA_VERSION=0.11.4-r1

RUN apk update
RUN apk add libressl${LIBRESSL_VERSION}-libcrypto libressl${LIBRESSL_VERSION}-libssl --update-cache --repository http://nl.alpinelinux.org/alpine/edge/main
RUN apk add librdkafka=${LIBRDKAFKA_VERSION} --update-cache --repository http://nl.alpinelinux.org/alpine/edge/community
RUN apk add librdkafka-dev=${LIBRDKAFKA_VERSION} --update-cache --repository http://nl.alpinelinux.org/alpine/edge/community
RUN apk add git openssh openssh-client openssl yajl-dev zlib-dev cyrus-sasl-dev openssl-dev build-base coreutils wget
```



To build the docker image with the library `librdkafka` :

```shell
docker build -t alpine-librdkafka-golang .
```

##### Publish

Then, we publish the image to [Docker Hub](https://hub.docker.com/r/drans/alpine-librdkafka-golang/) :

```shell
docker tag alpine-librdkafka-golang drans/alpine-librdkafka-golang:latest
docker push drans/alpine-librdkafka-golang:latest
```



#### Build

The consulting service is thought to be built and deploy with `Docker `,  the `Dockerfile` is :

```dockerfile
FROM drans/alpine-librdkafka-golang AS build-env

ADD . /go/src/consulting-service
WORKDIR /go/src/consulting-service
RUN go get -u github.com/golang/dep/cmd/dep
RUN dep ensure

RUN go build -o microservice

FROM drans/alpine-librdkafka-golang

WORKDIR /app
COPY --from=build-env /go/src/consulting-service/microservice /app/
ENTRYPOINT /app/microservice
EXPOSE 8080
```



To build the consulting  service :

```shell
docker build -t consulting-service .
```



#### Publish consulting service

##### Requirement

You need to be login to your docker registry to push the image on it `docker login $YOU_REGISTRY`

##### Publish

Before publishing, you need to define 2 arguments :

* The `tag` argument is the apply tag of all docker images 
* The  `registry` argument is the repository to push all docker images

To publish the docker image, you need to:

```shell
docker tag consulting-service $registry/consulting-service:$tag
docker push $registry/consulting-service:$tag
```

