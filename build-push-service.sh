#!/bin/sh
service=$1
tag=$2
registry=$3

echo "building service : $service"
docker build -t "$service" .
echo "service $service built"
docker tag "$service" "$registry/$service:$tag"
echo "pushing image $registry/$service:$tag"
docker push "$registry/$service:$tag"
echo "image $registry/$service:$tag pushed"
