package infrastructure

// Definition of all struct which represent HTTP request

type GetRoomBookingRequest struct {
	MeetingRoomName string 	`json:"meetingRoomName" valid:"matches(^[A-Z]{1}[0-9]{2}$),required"`
}

type HealthCheck struct {
	Status string `json:"status"`
}