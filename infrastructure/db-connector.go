package infrastructure

import (
	"database/sql"
	"time"
)

const (
	defaultConnectionAttemps         = 3
	defaultConnectionAttemptInterval = 10
)

var (
	defaultRetryPolicy = RetryPolicy{
		connectionAttempts:        defaultConnectionAttemps,
		connectionAttemptInterval: defaultConnectionAttemptInterval,
	}
)

func Connect(driver string, dns string, dbChannel chan *sql.DB, errorChannel chan error) {
	ConnectWithRetryPolicy(driver, dns, dbChannel, errorChannel, defaultRetryPolicy)
}

// Implement the mechanism of reach and retry to connect to the database
func ConnectWithRetryPolicy(driver string, dns string, dbChannel chan *sql.DB, errorChannel chan error, policy RetryPolicy) {
	logger.Debug("Entering ConnectWithRetryPolicy")
	var db *sql.DB
	var err error
	var attempts int

	for attempts = 0; attempts < policy.connectionAttempts; attempts++ {
		logger.Debugf("attempt [ %d ] to connect to database \n", attempts)
		db, err = sql.Open(driver, dns)
		if err == nil {
			logger.Debug("successfully connected to database")
			dbChannel <- db
			break
		}
		logger.Debug("waiting ", defaultConnectionAttemptInterval, " seconds before next attempt")
		time.Sleep(time.Second * defaultConnectionAttemptInterval)
	}
	if err != nil && db == nil {
		logger.Debug("cannot retrieve database connection")
		errorChannel <- err
	}
}

type RetryPolicy struct {
	connectionAttempts        int
	connectionAttemptInterval int
}
