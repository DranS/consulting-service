package infrastructure

import (
	"encoding/json"
	"consulting-service/usecase"
	"net/http"
	"github.com/asaskevich/govalidator"
)

// function to Handle and validate the HTTP request and the body JSON for the GetRoomBooking service
// Convert the HTTP request into a do domain struct
func HandleGetRoomBooking(service usecase.BookingManagementService) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		logger.Debug("Handle GetRoomBooking HTTP request")

		// parse request as GetRoomBookingRequest
		logger.Debug("parse request as GetRoomBookingRequest")
		addTokenRequest, err := decodeGetRoomBookingRequest(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}

		// validate request
		logger.Debug("validate request")
		valid, err := govalidator.ValidateStruct(addTokenRequest)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
		if !valid {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}


		roomBooking, err := service.GetRoomBooking(addTokenRequest.MeetingRoomName)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json.NewEncoder(w).Encode(roomBooking)
	}
}

// Convert HTTP request to struct Room Booking Request
func decodeGetRoomBookingRequest(r *http.Request) (*GetRoomBookingRequest, error) {
	var request GetRoomBookingRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return &request, nil
}

// function to Handle the Health check service
func HealtCheck(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	json.NewEncoder(w).Encode(HealthCheck{Status: "UP"})
}
