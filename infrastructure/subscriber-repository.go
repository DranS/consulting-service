package infrastructure

import (
	"database/sql"
	"errors"
	"consulting-service/domain"
	"consulting-service/log-factory"
	"github.com/satori/go.uuid"
)

var (
	logger = log_factory.LoggerFactory("repository")
)


// Implement of the "ConsultingRepository" interface from "domain" package


func NewConsultingRepository() domain.ConsultingRepository {
	return &dbConsultingRepository{}
}

// struct to access to the database
type dbConsultingRepository struct {
	*sql.DB
}

// function to start the connection with the database
func (d *dbConsultingRepository) Connect(dns string) {
	dbChannel := make(chan *sql.DB, 1)
	errorChannel := make(chan error, 1)
	go Connect("mysql", dns, dbChannel, errorChannel)
	select {
	case db := <-dbChannel:
		d.DB = db
	case err := <-errorChannel:
		logger.Errorf("ConsultingRepository database connection error : %s", err.Error())
	}

}

func (d *dbConsultingRepository) FindByBookingId(bookingID uuid.UUID) (booking *domain.Booking, err error) {
	stmt, err := d.Prepare(findByBookingId)

	if err != nil {
		return
	}
	defer stmt.Close()

	booking = &domain.Booking{}
	row := stmt.QueryRow(bookingID.Bytes())
	err = row.Scan(
		&booking.BookingID,
		&booking.StartingHour,
		&booking.EndingHour,
		&booking.TenantID,
		&booking.MeetingRoomName,
		&booking.UserEmail,
	)

	if err == sql.ErrNoRows {
		err = &domain.BookingNotFound{}
	}

	return
}

func (d *dbConsultingRepository) FindByMeetingRoomName(meetingRoomName string) (bookings []*domain.Booking, err error) {
	stmt, err := d.Prepare(findByMeetingRoomName)

	if err != nil {
		logger.Error(err.Error())
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query(meetingRoomName)
	if err != nil {
		logger.Error(err.Error())
		return
	}

	bookings = make([]*domain.Booking, 0)

	for rows.Next() {
		booking := new(domain.Booking)

		err = rows.Scan(
			&booking.BookingID,
			&booking.StartingHour,
			&booking.EndingHour,
			&booking.TenantID,
			&booking.MeetingRoomName,
			&booking.UserEmail,
		)
		if err != nil {
			logger.Error(err.Error())
		}
		bookings = append(bookings, booking)
	}
	if err := rows.Err(); err != nil {
		logger.Error(err.Error())
	}

	if err == sql.ErrNoRows {
		err = &domain.BookingNotFound{}
	}

	return
}

func (d *dbConsultingRepository) Save(booking *domain.Booking) (err error) {
	if booking == nil {
		err = errors.New("cannot save nil booking")
		logger.Error(err.Error())
		return
	}
	stmt, err := d.Prepare(saveBooking)
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(
		booking.BookingID.Bytes(),
		booking.StartingHour,
		booking.EndingHour,
		booking.TenantID,
		booking.MeetingRoomName,
		booking.UserEmail,
		booking.MeetingRoomName,
		booking.StartingHour,
		booking.EndingHour,
	)
	if err != nil {
		err = errors.New("cannot save booking")
		logger.Error(err.Error())
		return
	}
	return
}

func (d *dbConsultingRepository) Delete(bookingID uuid.UUID) (err error) {
	stmt, err := d.Prepare(deleteBooking)
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(bookingID.Bytes())
	if err != nil {
		err = errors.New("cannot delete booking")
		logger.Error(err.Error())
		return
	}
	return
}

const (
	findByBookingId       = "SELECT booking_id, starting_hour, ending_hour, tenant_id, meeting_room_name, user_email FROM booking WHERE booking_id = ?"
	findByMeetingRoomName = "SELECT booking_id, starting_hour, ending_hour, tenant_id, meeting_room_name, user_email FROM booking WHERE meeting_room_name = ?"
	saveBooking           = "INSERT INTO booking (booking_id, starting_hour, ending_hour, tenant_id, meeting_room_name, user_email) SELECT ?,?,?,?,?,? WHERE NOT EXISTS (SELECT booking_id from booking WHERE meeting_room_name = ? AND starting_hour = ? AND ending_hour = ?)"
	deleteBooking         = "DELETE FROM booking WHERE booking_id = ?"
)
