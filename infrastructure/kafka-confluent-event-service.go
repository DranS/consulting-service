package infrastructure

import (
	"fmt"
	"os"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"consulting-service/domain"
)

// Implement the Interface EventConsultingService to establish a connection to the kafka message broker and Handle the Event read from Subscribed topic

type kafkaConsultingService struct {
	brokerUrl string
	*kafka.Consumer
}

// Init the Kafka Consumer with all his properties
func NewKafkaConsultingService(brokerUrl string, groupId string) (domain.EventConsultingService, error) {
	var err error
	ks := kafkaConsultingService{
		brokerUrl: brokerUrl,
	}
	ks.Consumer, err = kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":               brokerUrl,
		"group.id":                        groupId,
		"session.timeout.ms":              60000,
		"go.events.channel.enable":        true,
		"go.application.rebalance.enable": true,
		"default.topic.config":            kafka.ConfigMap{"auto.offset.reset": "latest"}})
	return &ks, err
}

//Add or replace the given handler for a specific topic/event pair
func (ks *kafkaConsultingService) AddHandler(topic string, f func(data []byte) error) {
	go ks.subscribe(topic, f)
}

// Subcribe to kafka topics
// Call onMessageReceived for each message event read from kafka
func (ks kafkaConsultingService) subscribe(topic string, f func(data []byte) error) {
	err := ks.Subscribe(topic, nil)
	if err != nil {
		logger.Error(fmt.Sprintf("cannot subsribe to kafka topics : %s", err.Error()))
		return
	}
	for {
		ev := <-ks.Events()
		switch e := ev.(type) {
		case kafka.AssignedPartitions:
			//fmt.Fprintf(os.Stderr, "%% %v\n", e)
			ks.Assign(e.Partitions)
		case kafka.RevokedPartitions:
			//fmt.Fprintf(os.Stderr, "%% %v\n", e)
			ks.Unassign()
		case *kafka.Message:
			// Apply the Handler function for this topic
			err = f(e.Value)
			if err != nil {
				logger.Error(err.Error())
			}
		case kafka.PartitionEOF:
			//fmt.Printf("%% Reached %v\n", e)
		case kafka.Error:
			fmt.Fprintf(os.Stderr, "%% ErrorHTTP: %v\n", e)
		}
	}
	return
}
